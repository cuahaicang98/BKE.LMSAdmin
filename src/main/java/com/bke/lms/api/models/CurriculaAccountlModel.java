/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.CurriculaAccountDao;
import com.bke.bke.entity.CurriculaAccount;
import com.bke.common.utils.ConvertUtils;
import com.sun.net.httpserver.HttpExchange;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 *
 * @author User
 */
public class CurriculaAccountlModel extends _BaseModel<CurriculaAccount, CurriculaAccountDao> {
    
    public static final CurriculaAccountlModel INST = new CurriculaAccountlModel();

    @Override
    public CurriculaAccountDao getDao() {
        return CurriculaAccountDao.INST;
    }
    
    @Override
    public String doPost(HttpExchange exchange) {
        switch (methodName) {
            case "set-accounts":
                int curriculaId = ConvertUtils.toInteger(parameterMap.get("CurriculaID"));
                int type = ConvertUtils.toInteger(parameterMap.get("Type"));
                List<String> accountIds = ConvertUtils.strToListStr(parameterMap.get("AccountIDs"));
                
                getDao().delete("`CurriculaID` = " + curriculaId + " AND " + "`Type` = " + type);
                
                AtomicInteger rs = new AtomicInteger();
                accountIds.forEach(accountId -> {
                    List<String> fields = Arrays.asList("CurriculaID", "AccountID", "Type");
                    List<String> values = Arrays.asList(
                            ConvertUtils.toString(curriculaId),
                            ConvertUtils.toString(accountId),
                            ConvertUtils.toString(type)
                    );
                    
                    rs.addAndGet(getDao().create(fields, values));
                });
                
                return "" + rs.intValue();
        }

        return super.doPost(exchange);
    }
}
