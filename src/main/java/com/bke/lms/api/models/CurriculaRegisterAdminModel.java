/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.CurriculaAccountDao;
import com.bke.bke.dao.ProcedureDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Procedures;
import com.bke.common.utils.ConvertUtils;
import com.bke.lms.common.CurriculaAccountType;
import com.bke.lms.common.ProcedureStatus;
import com.bke.lms.common.ProcedureType;
import com.bke.lms.utils.ExchangeUtils;
import com.sun.net.httpserver.HttpExchange;
import java.util.Arrays;

/**
 *
 * @author User
 */
public class CurriculaRegisterAdminModel extends _BaseModel<Procedures, ProcedureDao> {

    public static final CurriculaRegisterAdminModel INST = new CurriculaRegisterAdminModel();

    @Override
    public ProcedureDao getDao() {
        return ProcedureDao.INST;
    }

    @Override
    public void parseExchange(HttpExchange exchange) {
        super.parseExchange(exchange);
        parameterMap.put("Type", ConvertUtils.toString(ProcedureType.CURRICULA_REGISTER.value));
    }

    @Override
    public String doPost(HttpExchange exchange) {
        Account account = ExchangeUtils.getAccount(exchange);
        String targetId = parameterMap.get("TargetID");
        String whereClause = "`AccountID` = " + account.AccountID + " AND `TargetID` = " + targetId;

        switch (methodName) {
            case "approve": {
                int rs = getDao().executeUpdate("UPDATE `Procedures` SET `Status` = " + ProcedureStatus.APPROVED.value + " WHERE " + whereClause);
                
                if (rs > 0) {
                    CurriculaAccountDao.INST.create(
                            Arrays.asList("CurriculaID", "AccountID", "Type"),
                            Arrays.asList("" + targetId, "" + account.AccountID, "" + CurriculaAccountType.STUDENT.value)
                    );
                }
                
                return "" + rs;
            }
            case "reject": {
                int rs = getDao().executeUpdate("UPDATE `Procedures` SET `Status` = " + ProcedureStatus.REJECTED.value + " WHERE " + whereClause);
                
                if (rs > 0) {
                    CurriculaAccountDao.INST.delete(String.format("`CurriculaID` = %s AND `AccountID` = %s AND `Type` = %s", targetId, account.AccountID, CurriculaAccountType.STUDENT.value));
                }
                
                return "" + rs;
            }
        }

        return super.doPost(exchange);
    }
}
