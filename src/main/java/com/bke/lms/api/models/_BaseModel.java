/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao._EntityDao;
import static com.bke.bke.dao._EntityDao.parseField;
import static com.bke.bke.dao._EntityDao.parseValue;
import com.bke.bke.entity._EntityBase;
import com.bke.common.utils.ConvertUtils;
import com.bke.common.utils.JsonUtils;
import com.bke.lms.common.Constants;
import com.bke.lms.utils.ExchangeUtils;
import com.sun.net.httpserver.HttpExchange;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.stream.Collectors;

/**
 *
 * @author User
 */
public abstract class _BaseModel<E extends _EntityBase, D extends _EntityDao> {
    
    public String methodType;
    public String methodName;

    public Map<String, String> parameterMap;
    public int offset;
    public int limit;
    
    public abstract D getDao();
    
    public String doProcess(HttpExchange exchange) {
        parseExchange(exchange);
        
        switch (methodType) {
            case "get":
                return doGet(exchange);
            case "post":
                return doPost(exchange);
        }
        
        return "";
    }
    
    public String doGet(HttpExchange exchange) {
        switch (methodName) {
            case "items":
                return handleGetItems(exchange);
        }
        
        return "";
    }
    
    public String doPost(HttpExchange exchange) {
        switch (methodName) {
            case "create":
                return handlePostCreate(exchange);
            case "update":
                return handlePostUpdate(exchange);
            case "delete":
                return handlePostDelete(exchange);
        }
        
        return "";
    }
    
    public String handleGetItems(HttpExchange exchange) {
        List<String> keys = new ArrayList<>(parameterMap.keySet());
        List<String> values = keys.stream().map(parameterMap::get).collect(Collectors.toList());
        
        List<E> items = getDao().getList(offset, limit, keys, values);
        return JsonUtils.toJson(items);
    }
    
    public String handlePostCreate(HttpExchange exchange) {
        List<String> keys = new ArrayList<>(parameterMap.keySet());
        List<String> values = keys.stream().map(parameterMap::get).collect(Collectors.toList());

        return ConvertUtils.toString(getDao().create(keys, values));
    }
    
    public String handlePostUpdate(HttpExchange exchange) {
        List<String> keys = new ArrayList<>(parameterMap.keySet());
        List<String> values = keys.stream().map(parameterMap::get).collect(Collectors.toList());

        return ConvertUtils.toString(getDao().update(keys, values));
    }
    
    public String handlePostDelete(HttpExchange exchange) {
        List<String> builder = new ArrayList<>();

        parameterMap.keySet().forEach((key) -> {
            String value = parameterMap.get(key);
            
            builder.add(_EntityDao.parseField(key) + " = " + _EntityDao.parseValue(value));
        });

        String whereClause = String.join(" AND ", builder);
        return ConvertUtils.toString(getDao().delete(whereClause));
    }
    
    public void parseExchange(HttpExchange exchange) {
        Matcher matcher = Constants.RESOURCE_PATTERN.matcher(exchange.getRequestURI().toString());

        if (matcher.find() && matcher.find() && matcher.find()) {
            methodType = matcher.group(0).replaceFirst("/", "");
            
            if (matcher.find()) {
                methodName = matcher.group(0).replaceFirst("/", "");
            }
        }
        
        parameterMap = ExchangeUtils.getParamterMap(exchange);
        offset = ConvertUtils.toInteger(parameterMap.remove("offset"), 0);
        limit = ConvertUtils.toInteger(parameterMap.remove("offset"), Integer.MAX_VALUE);
    }
    
    public String buildWhereClase() {
        return buildWhereClause(parameterMap);
    }
    
    public String buildWhereClause(Map<String, String> map) {
        List<String> fields = map.keySet().stream().map(field -> parseField(field)).collect(Collectors.toList());
        List<String> values = map.values().stream().map(value -> parseValue(value)).collect(Collectors.toList());
        
        List<String> builder = new ArrayList<>(Arrays.asList("1 = 1"));
        
        for (int i = 0; i < fields.size(); i++) {
            builder.add(fields.get(i) + " = " + values.get(i));
        }
        
        return String.join(" AND ", builder);
    }
}
