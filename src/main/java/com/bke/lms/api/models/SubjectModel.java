/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.SubjectDao;
import com.bke.bke.entity.Subject;

/**
 *
 * @author User
 */
public class SubjectModel extends _BaseModel<Subject, SubjectDao> {
    
    public static final SubjectModel INST = new SubjectModel();

    @Override
    public SubjectDao getDao() {
        return SubjectDao.INST;
    }
}
