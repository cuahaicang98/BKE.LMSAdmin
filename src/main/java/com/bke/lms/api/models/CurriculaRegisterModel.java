/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.ProcedureDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Procedures;
import com.bke.common.utils.ConvertUtils;
import com.bke.lms.common.ProcedureStatus;
import com.bke.lms.common.ProcedureType;
import com.bke.lms.utils.ExchangeUtils;
import com.sun.net.httpserver.HttpExchange;
import java.util.Arrays;


/**
 *
 * @author User
 */
public class CurriculaRegisterModel extends _BaseModel<Procedures, ProcedureDao> {
    
    public static final CurriculaRegisterModel INST = new CurriculaRegisterModel();

    @Override
    public ProcedureDao getDao() {
        return ProcedureDao.INST;
    }
    
    @Override
    public void parseExchange(HttpExchange exchange) {
        super.parseExchange(exchange);
        parameterMap.put("Type", ConvertUtils.toString(ProcedureType.CURRICULA_REGISTER.value));
    }
    
    @Override
    public String doPost(HttpExchange exchange) {
        Account account = ExchangeUtils.getAccount(exchange);
        String curriculaId = parameterMap.get("CurriculaID");
        String whereClause = "`AccountID` = " + account.AccountID + " AND `TargetID` = " + curriculaId;

        switch (methodName) {
            case "register": {
                if (getDao().exists(whereClause)) {
                    return "" + getDao().executeUpdate("UPDATE `Procedures` SET `Status` = " + ProcedureStatus.PENDING.value + " WHERE " + whereClause);
                } else {
                    return "" + getDao().create(
                            Arrays.asList("AccountID", "Type", "TargetID", "Status"),
                            Arrays.asList("" + account.AccountID, "" + ProcedureType.CURRICULA_REGISTER.value, curriculaId, "" + ProcedureStatus.PENDING.value)
                    );
                }
            }
            case "unregister": {
                return "" + getDao().executeUpdate("UPDATE `Procedures` SET `Status` = " + ProcedureStatus.OTHERS.value + " WHERE " + whereClause);
            }
        }
        
        return super.doPost(exchange);
    }
}
