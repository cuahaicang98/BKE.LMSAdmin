/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.GroupDao;
import com.bke.bke.entity.Group;

/**
 *
 * @author User
 */
public class GroupModel extends _BaseModel<Group, GroupDao> {
    
    public static final GroupModel INST = new GroupModel();

    @Override
    public GroupDao getDao() {
        return GroupDao.INST;
    }
}
