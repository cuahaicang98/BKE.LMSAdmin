/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.CurriculaDao;
import com.bke.bke.entity.Curricula;

/**
 *
 * @author User
 */
public class CurriculaModel extends _BaseModel<Curricula, CurriculaDao> {
    
    public static final CurriculaModel INST = new CurriculaModel();

    @Override
    public CurriculaDao getDao() {
        return CurriculaDao.INST;
    }
}
