/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.entity.Account;
import com.bke.common.utils.SessionUtils;
import com.sun.net.httpserver.HttpExchange;

/**
 *
 * @author User
 */
public class AuthModel extends _BaseModel<Account, AccountDao> {
    
    public static final AuthModel INST = new AuthModel();

    @Override
    public AccountDao getDao() {
        return AccountDao.INST;
    }
    
    @Override
    public String doGet(HttpExchange exchange) {
        return "";
    }
    
    @Override
    public String doPost(HttpExchange exchange) {
        switch (methodName) {
            case "login": {
                boolean exists = getDao().exists(buildWhereClase());
                return exists ? SessionUtils.generate(parameterMap.get("Username")) : "";
            }
        }
        
        return "";
    }
}
