/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.models;

import com.bke.bke.dao.CurriculaDetailDao;
import com.bke.bke.entity.CurriculaDetail;

/**
 *
 * @author User
 */
public class CurriculaDetailModel extends _BaseModel<CurriculaDetail, CurriculaDetailDao> {
    
    public static final CurriculaDetailModel INST = new CurriculaDetailModel();

    @Override
    public CurriculaDetailDao getDao() {
        return CurriculaDetailDao.INST;
    }
}
