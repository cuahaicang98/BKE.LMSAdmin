/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.requests;

import com.bke.lms.api.models.SubjectModel;
import com.sun.net.httpserver.HttpExchange;

/**
 *
 * @author User
 */
public class SubjectRequest extends _BaseRequest {

    @Override
    public String doProcess(HttpExchange exchange) {
        return SubjectModel.INST.doProcess(exchange);
    }
}
