/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.api.requests;

import com.bke.common.utils.JsonUtils;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public abstract class _BaseRequest {
    
    public abstract String doProcess(HttpExchange exchange);
    
    public void doHandler(HttpExchange exchange) {
        try {
            response(exchange, 200, doProcess(exchange));
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    protected void response(HttpExchange exchange, int status, String response) {
        try {
            try (OutputStream output = exchange.getResponseBody()) {
                Map<String, Object> resp = new HashMap<>();
                resp.put("data", response);
                resp.put("status", 200);
                resp.put("server_time", System.currentTimeMillis());
                
                byte[] bytes = JsonUtils.toJson(resp).getBytes();
                
                exchange.getResponseHeaders().add("Content-Type", "application/json");
                exchange.sendResponseHeaders(status, bytes.length);
                output.write(bytes);
                output.flush();
            }
        } catch (IOException e) {
            Logger.getLogger(_BaseRequest.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
