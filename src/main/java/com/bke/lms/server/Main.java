package com.bke.lms.server;

import com.bke.lms.common.Constants;
import com.bke.lms.handlers.*;
import com.sun.net.httpserver.HttpServer;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import org.rythmengine.Rythm;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author User
 */
public class Main {

    public static void main(String[] args) throws IOException {
        initRythm();

        HttpServer server = HttpServer.create(new InetSocketAddress(Constants.PORT), 0);
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(10);
        
        server.createContext("/", new RouteHandler());
        server.createContext("/api", new ApiRouteHandler());
        server.setExecutor(executor);
        server.start();
    }
    
    private static void initRythm() {
        Map<String, Object> conf = new HashMap<>();
        conf.put("home.template", new File("./resource"));
        Rythm.init(conf);
    }
}
