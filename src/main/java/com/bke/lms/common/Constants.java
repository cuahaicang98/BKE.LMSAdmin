/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.common;

import com.bke.common.utils.ConfigUtils;
import java.util.regex.Pattern;

/**
 *
 * @author User
 */
public class Constants {
    
    public static final String HOST = ConfigUtils.INST.readString("host");
    public static final int PORT = ConfigUtils.INST.readInteger("port");
    
    public static final Pattern RESOURCE_PATTERN = Pattern.compile("(/[a-zA-Z0-9-]+)");
    
    public static final String TEMPLATE_ACCOUNT_PAGE = "/html/account.html";
    public static final String TEMPLATE_CURRICULA_PAGE = "/html/curricula.html";
    public static final String TEMPLATE_CURRICULA_DETAILS_PAGE = "/html/curricula-detail.html";
    public static final String TEMPLATE_CURRICULA_REGISTER_PAGE = "/html/curricula-register.html";
    public static final String TEMPLATE_CURRICULA_REGISTER_ADMIN_PAGE = "/html/curricula-register-admin.html";
    public static final String TEMPLATE_ERROR_PAGE = "/html/error.html";
    public static final String TEMPLATE_FORGOT_PASSWORD_PAGE = "/html/forgot-password.html";
    public static final String TEMPLATE_GROUP_PAGE = "/html/group.html";
    public static final String TEMPLATE_HOME_PAGE = "/html/index.html";
    public static final String TEMPLATE_LOGIN_PAGE = "/html/login.html";
    public static final String TEMPLATE_PROFILE_PAGE = "/html/profile.html";
    public static final String TEMPLATE_SUBJECT_PAGE = "/html/subject.html";
    public static final String TEMPLATE_TIME_TABLE_PAGE = "/html/time-table.html";
}
