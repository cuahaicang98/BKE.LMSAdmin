/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.common;

/**
 *
 * @author User
 */
public enum CurriculaAccountType {
    STUDENT(1),
    TEACHER(2);
    
    public final int value;
    
    private CurriculaAccountType(int value) {
        this.value = value;
    }
}
