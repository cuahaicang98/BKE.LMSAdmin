/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.common;

/**
 *
 * @author User
 */
public enum ProcedureStatusLabel {
    OTHERS("Chưa đăng kí"),
    PENDING("Đang chờ"),
    APPROVED("Đã đăng kí"),
    REJECTED("Đã từ chối"),
    CANCEL_PENDING("Đang hủy"),
    CANCELED("Đã hủy");
    
    public final String value;
    
    private ProcedureStatusLabel(String value) {
        this.value = value;
    }
    
    public static String getLabel(int status) {
        switch (status) {
            case 1:
                return ProcedureStatusLabel.PENDING.value;
            case 2:
                return ProcedureStatusLabel.APPROVED.value;
            case 3:
                return ProcedureStatusLabel.REJECTED.value;
            case 4:
                return ProcedureStatusLabel.CANCEL_PENDING.value;
            case 5:
                return ProcedureStatusLabel.CANCELED.value;
            default:
                return ProcedureStatusLabel.OTHERS.value;
        }
    }
    
    public static String getLabel(ProcedureStatus status) {
        switch (status) {
            case PENDING:
                return ProcedureStatusLabel.PENDING.value;
            case APPROVED:
                return ProcedureStatusLabel.APPROVED.value;
            case REJECTED:
                return ProcedureStatusLabel.REJECTED.value;
            case CANCEL_PENDING:
                return ProcedureStatusLabel.CANCEL_PENDING.value;
            case CANCELED:
                return ProcedureStatusLabel.CANCELED.value;
            default:
                return ProcedureStatusLabel.OTHERS.value;
        }
    }
}
