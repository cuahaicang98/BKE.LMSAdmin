/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.common;

/**
 *
 * @author User
 */
public enum ProcedureType {
    CURRICULA_REGISTER(1);
    
    public final int value;
    
    private ProcedureType(int value) {
        this.value = value;
    }
}
