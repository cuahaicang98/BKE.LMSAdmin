/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.common;

/**
 *
 * @author User
 */
public enum ProcedureStatus {
    OTHERS(0),
    PENDING(1),
    APPROVED(2),
    REJECTED(3),
    CANCEL_PENDING(4),
    CANCELED(5);
    
    public final int value;
    
    private ProcedureStatus(int value) {
        this.value = value;
    }
}
