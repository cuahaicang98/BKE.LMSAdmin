/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.common;

import com.bke.common.utils.ConvertUtils;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author User
 */
public class Pagination {

    public int currentPage;
    public int totalPage;
    
    public boolean hasPrev;
    public boolean hasNext;
    public boolean hasPrevFirst;
    public boolean hasNextLast;
    
    public List<Integer> pages;

    private Pagination() {
    }

    private Pagination(int currentPage, int total) {
        this.currentPage = currentPage;
        this.totalPage = ConvertUtils.toInteger(Math.ceil(1.0 * total / 20), 1);
        
        this.hasPrev = currentPage != 1;
        this.hasNext = currentPage != this.totalPage;
        this.hasPrevFirst = this.hasPrev && this.totalPage > 5 && currentPage > 3;
        this.hasNextLast = this.hasNext && this.totalPage > 5 && (currentPage < this.totalPage - 2);
        
        int startPage = (currentPage - 2) > 0 ? (currentPage - 2) : 1;
        if (this.totalPage > 5 && currentPage >= totalPage - 2) {
            startPage = totalPage - 4;
        }
        
        pages = new ArrayList<>();
        for (int i = startPage; i <= totalPage && i < startPage + 5; i++) {
            pages.add(i);
        }
    }
    
    public static Pagination buildPagination(int currentPage, int total) {
        if (total <= 20) {
            Pagination pagination = new Pagination();
            pagination.totalPage = 0;
            
            return pagination;
        }
        
        return new Pagination(currentPage, total);
    }
}
