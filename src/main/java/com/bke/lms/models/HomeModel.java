/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.entity.Account;
import com.bke.lms.common.Constants;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class HomeModel extends _BaseModel {
    
    public static final HomeModel INST = new HomeModel();
    
    public String buildHomePage(Account account) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Trang chủ");
        params.put("account", account);
        
        return Rythm.render(Constants.TEMPLATE_HOME_PAGE, params);
    }
}
