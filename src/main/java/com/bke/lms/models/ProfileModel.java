/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.entity.Account;
import com.bke.lms.common.Constants;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class ProfileModel extends _BaseModel {
    
    public static final ProfileModel INST = new ProfileModel();
    
    public String buildProfilePage(Account account) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Thông tin cá nhân");
        params.put("account", account);

        return Rythm.render(Constants.TEMPLATE_PROFILE_PAGE, params);
    }
}
