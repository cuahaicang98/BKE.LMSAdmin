/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.SubjectDao;
import com.bke.bke.entity.Account;
import com.bke.lms.common.Constants;
import com.bke.lms.common.Pagination;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class SubjectModel extends _BaseModel {
    
    public static final SubjectModel INST = new SubjectModel();
    
    public String buildSubjectPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Quản lý môn học");
        params.put("account", account);

        params.put("subjects", SubjectDao.INST.getList(20 * (page - 1), 20, "`CreatedDate` DESC"));
        params.put("pagination", Pagination.buildPagination(page, SubjectDao.INST.count()));

        return Rythm.render(Constants.TEMPLATE_SUBJECT_PAGE, params);
    }
}
