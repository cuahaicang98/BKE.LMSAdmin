/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.entity.Account;
import com.bke.lms.common.Constants;
import com.bke.lms.common.Pagination;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class AccountModel extends _BaseModel {
    
    public static final AccountModel INST = new AccountModel();
    
    public String buildAccountPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Quản lý tài khoản");
        params.put("account", account);
        
        List<Account> accounts = AccountDao.INST.getList(20 * (page - 1), 20, "`AccountID` ASC");
        params.put("accounts", accounts);
        params.put("pagination", Pagination.buildPagination(page, AccountDao.INST.count()));

        return Rythm.render(Constants.TEMPLATE_ACCOUNT_PAGE, params);
    }
}
