/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.dao.CurriculaDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Curricula;
import com.bke.lms.common.Constants;
import com.bke.lms.common.Pagination;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class CurriculaModel extends _BaseModel {
    
    public static final CurriculaModel INST = new CurriculaModel();
    
    public String buildCurriculaPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Quản lý chương trình");
        params.put("account", account);
        
        List<Curricula> curriculas = CurriculaDao.INST.getList(20 * (page - 1), 20, "`CurriculaID` DESC");
        params.put("curriculas", curriculas);
        params.put("pagination", Pagination.buildPagination(page, CurriculaDao.INST.count()));
        
        List<Account> accounts = AccountDao.INST.getList(0, Integer.MAX_VALUE, "`AccountID` ASC");
        params.put("accounts", accounts);

        return Rythm.render(Constants.TEMPLATE_CURRICULA_PAGE, params);
    }
}
