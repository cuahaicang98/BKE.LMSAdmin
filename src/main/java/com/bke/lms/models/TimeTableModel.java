/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.CurriculaDetailDao;
import com.bke.bke.entity.Account;
import com.bke.common.utils.ConvertUtils;
import com.bke.common.utils.DateTimeUtils;
import com.bke.lms.common.Constants;
import com.bke.lms.common.CurriculaAccountType;
import com.bke.lms.utils.ExchangeUtils;
import com.google.gson.internal.LinkedTreeMap;
import com.sun.net.httpserver.HttpExchange;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class TimeTableModel extends _BaseModel {
    
    public static final TimeTableModel INST = new TimeTableModel();
    private final String[] dayOfWeeks = new String[] {
        "Thứ 2", "Thứ 3", "Thứ 4", "Thứ 5", "Thứ 6", "Thứ 7", "C/Nhật", 
    };
    
    public String buildTimeTablePage(HttpExchange exchange, Account account) {
        Map<String, String> paramterMap = ExchangeUtils.getParamterMap(exchange);
        
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Lịch học");
        params.put("account", account);
        
        String time = getTime(paramterMap);
        params.put("time", time);
        params.put("columns", buildColumns(getStartDate(time)));
        
        List<Map> items = CurriculaDetailDao.INST.getAsListMap(String.format(
                "SELECT c.SubjectID, cd.StartTime, ca.Type "
                        + "FROM CurriculaDetails cd JOIN Curriculas c ON cd.CurriculaID = c.CurriculaID JOIN CurriculaAccounts ca ON cd.CurriculaID = ca.CurriculaID "
                        + "WHERE cd.StartTime BETWEEN '%s' AND '%s' AND ca.AccountID = %d "
                        + "ORDER BY StartTime ASC;",
                DateTimeUtils.format(getStartDate(time), "yyyy-MM-dd 00:00"),
                DateTimeUtils.format(getEndDate(time), "yyyy-MM-dd 23:59"),
                account.AccountID
        ));
        
        params.put("items", normalizeItems(items));
        
        return Rythm.render(Constants.TEMPLATE_TIME_TABLE_PAGE, params);
    }
    
    private LinkedTreeMap<String, List<String>> normalizeItems(List<Map> items) {
        LinkedTreeMap<String, List<String>> normalizedItems = new LinkedTreeMap<>();

        for (Map item : items) {
            Timestamp startTime = ConvertUtils.toTimestamp(item.get("StartTime"));
            
            Calendar cal = Calendar.getInstance();
            cal.setTime(startTime);
            int startDay = cal.get(Calendar.DAY_OF_WEEK) - 2;
            
            item.put("StartDay", startDay >= 0 ? startDay : 6);
            item.put("StartTimeFormat", DateTimeUtils.format(startTime.getTime(), "HH:mm"));
        }

        List<String> hours = items.stream()
                .map(item -> ConvertUtils.toString(item.get("StartTimeFormat")))
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        
        for (String hour : hours) {
            List<String> values = new ArrayList<>();
            
            for (int day = 0; day < 7; day++) {
                String dayString = "" + day;
                String value = items.stream()
                        .filter(item -> {
                            int startDay = ConvertUtils.toInteger(item.get("StartDay"));
                            String startTimeFormat = ConvertUtils.toString(item.get("StartTimeFormat"));
                            
                            return startTimeFormat.equals(hour) && dayString.equals("" + startDay);
                        })
                        .map(item -> {
                            String subjectId = ConvertUtils.toString(item.get("SubjectID"));
                            int type = ConvertUtils.toInteger(item.get("Type"));
                            
                            if (type == CurriculaAccountType.STUDENT.value) {
                                return subjectId + " - Học viên";
                            }
                            
                            return subjectId + " - Giảng viên";
                        }).collect(Collectors.joining("<br />"));
                
                values.add(value);
            }
            
            normalizedItems.put(hour, values);
        }
        
        return normalizedItems;
    }
    
    private List<String> buildColumns(Date startDate) {
        List<String> columns = new ArrayList<>();

        for (int i = 0; i < 7; i++) {
            Date date = DateTimeUtils.addDays(startDate, i);
            columns.add(dayOfWeeks[i] + " - " + DateTimeUtils.format(date, "dd/MM"));
        }
        
        return columns;
    }
    
    private String getTime(Map<String, String> paramterMap) {
        if (paramterMap.containsKey("time")) {
            return paramterMap.get("time");
        }

        int year = DateTimeUtils.getYear();
        int week = DateTimeUtils.getWeek();
        
        return year + "-W" + week;
    }
    
    private Date getStartDate(String time) {
        int year = ConvertUtils.toInteger(time.split("-W")[0]);
        int week = ConvertUtils.toInteger(time.split("-W")[1]);
        
        Calendar cal = Calendar.getInstance();
        cal.setWeekDate(year, week, Calendar.MONDAY);
        
        return cal.getTime();
    }
    
    private Date getEndDate(String time) {
        int year = ConvertUtils.toInteger(time.split("-W")[0]);
        int week = ConvertUtils.toInteger(time.split("-W")[1]);
        
        Calendar cal = Calendar.getInstance();
        cal.setWeekDate(year, week, Calendar.MONDAY);
        cal.add(Calendar.DATE, 6);
        
        return cal.getTime();
    }
}
