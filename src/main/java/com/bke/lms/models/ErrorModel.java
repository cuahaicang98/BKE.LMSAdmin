/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.lms.common.Constants;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class ErrorModel extends _BaseModel {
    
    public static final ErrorModel INST = new ErrorModel();
    
    public String buildErrorPage() {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Có lỗi xảy ra");

        return Rythm.render(Constants.TEMPLATE_ERROR_PAGE, params);
    }
}
