/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.dao.CurriculaAccountDao;
import com.bke.bke.dao.CurriculaDao;
import com.bke.bke.dao.ProcedureDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Curricula;
import com.bke.bke.entity.CurriculaAccount;
import com.bke.lms.common.Constants;
import com.bke.lms.common.CurriculaAccountType;
import com.bke.lms.common.Pagination;
import com.bke.lms.common.ProcedureStatus;
import com.bke.lms.common.ProcedureType;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class CurriculaRegisterModel extends _BaseModel {
    
    public static final CurriculaRegisterModel INST = new CurriculaRegisterModel();
    
    public String buildCurriculaRegisterPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Đăng kí học phần");
        params.put("account", account);
        
        List<Curricula> curriculas = CurriculaDao.INST.getList("`StartTime` > CURDATE()", 20 * (page - 1), 20, "`StartTime` DESC");
        params.put("curriculas", curriculas);
        params.put("mapCurriculaAccount", getMapCurriculaAccount(curriculas));
        params.put("mapStatus", getMapStatus(curriculas));
        params.put("pagination", Pagination.buildPagination(page, CurriculaDao.INST.count("`StartTime` > CURDATE()")));
        
        return Rythm.render(Constants.TEMPLATE_CURRICULA_REGISTER_PAGE, params);
    }
    
    private Map<Integer, String> getMapCurriculaAccount(List<Curricula> curriculas) {
        String curriculaIds = curriculas.stream().map(curricula -> "" + curricula.CurriculaID).collect(Collectors.joining(","));
        List<CurriculaAccount> curriculaAccounts = CurriculaAccountDao.INST.getList("`CurriculaID` IN (" + curriculaIds + ") AND `Type` = " + CurriculaAccountType.TEACHER.value, 0, Integer.MAX_VALUE);
        
        String accountIds = curriculaAccounts.stream().map(ca -> "" + ca.AccountID).collect(Collectors.joining(","));
        Map<Integer, String> mapAccount = AccountDao.INST.getList("`AccountID` IN (" + accountIds + ")", 0, Integer.MAX_VALUE).stream()
                .collect(Collectors.toMap(
                        account -> account.AccountID,
                        account -> String.format("%s %s", account.FirstName, account.LastName)
                ));
        
        Map<Integer, String> mapCurriculaAccount = new HashMap<>();
        for (CurriculaAccount ca : curriculaAccounts) {
            String account = mapAccount.get(ca.AccountID);

            if (mapCurriculaAccount.containsKey(ca.CurriculaID)) {
                mapCurriculaAccount.put(ca.CurriculaID, mapCurriculaAccount.get(ca.CurriculaID) + ", " + account);
            } else {
                mapCurriculaAccount.put(ca.CurriculaID, account);
            }
        }
        
        return mapCurriculaAccount;
    }
    
    private Map<Integer, Integer> getMapStatus(List<Curricula> curriculas) {
        String curriculaIds = curriculas.stream().map(curricula -> "" + curricula.CurriculaID).collect(Collectors.joining(","));
        Map<Integer, Integer> mapProcedure = ProcedureDao.INST.getList("`TargetID` IN (" + curriculaIds + ") AND Type = " + ProcedureType.CURRICULA_REGISTER.value , 0, Integer.MAX_VALUE)
                .stream()
                .collect(Collectors.toMap(
                        procedure -> procedure.TargetID,
                        procedure -> procedure.Status
                ));
        
        Map<Integer, Integer> mapStatus = new HashMap<>();
        for (Curricula curricula : curriculas) {
            mapStatus.put(curricula.CurriculaID, mapProcedure.getOrDefault(curricula.CurriculaID, ProcedureStatus.OTHERS.value));
        }
        
        return mapStatus;
    }
}
