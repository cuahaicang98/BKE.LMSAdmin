/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.CurriculaDao;
import com.bke.bke.dao.CurriculaDetailDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Curricula;
import com.bke.bke.entity.CurriculaDetail;
import com.bke.common.utils.ConvertUtils;
import com.bke.lms.common.Constants;
import com.bke.lms.common.Pagination;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class CurriculaDetailModel extends _BaseModel {
    
    public static final CurriculaDetailModel INST = new CurriculaDetailModel();
    
    public String buildCurriculaDetailPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Quản lý lịch học");
        params.put("account", account);
        
        List<CurriculaDetail> curriculaDetails = CurriculaDetailDao.INST.getList(20 * (page - 1), 20, "`StartTime` DESC, `CurriculaID` DESC");
        params.put("details", curriculaDetails);
        params.put("pagination", Pagination.buildPagination(page, CurriculaDetailDao.INST.count()));
        
        List<String> curriculaIds = curriculaDetails.stream().map(detail -> ConvertUtils.toString(detail.CurriculaID)).distinct().collect(Collectors.toList());
        List<Curricula> curriculas = CurriculaDao.INST.getList("`CurriculaID` IN (" + String.join(", ", curriculaIds) + ")", 0, Integer.MAX_VALUE);
        
        Map<Integer, String> mapCurricula = curriculas.stream()
                .collect(Collectors.toMap(
                        curricula -> curricula.CurriculaID,
                        curricula -> curricula.SubjectID
                ));
        
        params.put("mapCurricula", mapCurricula);

        return Rythm.render(Constants.TEMPLATE_CURRICULA_DETAILS_PAGE, params);
    }
}
