/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.lms.common.Constants;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class LoginModel extends _BaseModel {
    
    public static final LoginModel INST = new LoginModel();
    
    public String buildLoginPage() {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Đăng nhập");

        return Rythm.render(Constants.TEMPLATE_LOGIN_PAGE, params);
    }
}
