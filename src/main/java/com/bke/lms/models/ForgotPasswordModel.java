/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.lms.common.Constants;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class ForgotPasswordModel extends _BaseModel {
    
    public static final ForgotPasswordModel INST = new ForgotPasswordModel();
    
    public String buildForgotPasswordPage() {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Quên mật khẩu");

        return Rythm.render(Constants.TEMPLATE_FORGOT_PASSWORD_PAGE, params);
    }
    
}
