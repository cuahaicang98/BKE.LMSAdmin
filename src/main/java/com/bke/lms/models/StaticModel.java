/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author User
 */
public class StaticModel extends _BaseModel {
    
    public static final StaticModel INST = new StaticModel();
    
    public Pair<String, byte[]> getResource(String filePath) {
        try {
            File file = new File(System.getProperty("user.dir") + filePath);
            String extension = FilenameUtils.getExtension(file.getName());
            
            if (!file.exists() || !file.isFile()) {
                throw new IllegalArgumentException();
            }
            
            byte[] resource = Files.readAllBytes(file.toPath());
            
            switch (extension) {
                case "css": case "scss":
                    return Pair.of("text/css", resource);
                case "js":
                    return Pair.of("text/javascript", resource);
                case "jpg": case "jpeg": case "png": case "ico":
                    return Pair.of("image/" + extension, resource);
                case "woff": case "woff2": case "ttf":
                    return Pair.of("application/octet-stream", resource);
                default:
                    throw new IllegalArgumentException();
            }
        } catch (IOException ex) {
            return null;
        }
    }
}
