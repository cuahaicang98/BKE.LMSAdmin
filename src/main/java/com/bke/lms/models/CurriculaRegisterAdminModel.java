/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.dao.CurriculaDao;
import com.bke.bke.dao.ProcedureDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Procedures;
import com.bke.common.utils.DateTimeUtils;
import com.bke.lms.common.Constants;
import com.bke.lms.common.Pagination;
import com.bke.lms.common.ProcedureStatus;
import com.bke.lms.common.ProcedureType;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.apache.commons.lang3.tuple.Pair;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class CurriculaRegisterAdminModel extends _BaseModel {
    
    public static final CurriculaRegisterAdminModel INST = new CurriculaRegisterAdminModel();
    
    public String buildCurriculaRegisterAdminPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Đăng kí học phần");
        params.put("account", account);
        
        List<String> filterStatus = Arrays.asList(
                "" + ProcedureStatus.PENDING.value,
                "" + ProcedureStatus.APPROVED.value,
                "" + ProcedureStatus.REJECTED.value
        );
        String whereClause = "`Type` = " + ProcedureType.CURRICULA_REGISTER.value + " AND `Status` IN (" + String.join(",", filterStatus) + ")";
        
        List<Procedures> procedures = ProcedureDao.INST.getList(whereClause, 20 * (page - 1), 20);
        params.put("procedures", procedures);
        params.put("mapCurricula", getMapCurricula(procedures));
        params.put("mapAccount", getMapAccount(procedures));
        params.put("pagination", Pagination.buildPagination(page, ProcedureDao.INST.count(whereClause)));
        
        return Rythm.render(Constants.TEMPLATE_CURRICULA_REGISTER_ADMIN_PAGE, params);
    }
    
    private Map<Integer, Pair<String, String>> getMapCurricula(List<Procedures> procedures) {
        String curriculaIds = procedures.stream().map(procedure -> "" + procedure.TargetID).collect(Collectors.joining(","));
        
        return CurriculaDao.INST.getList("`CurriculaID` IN (" + curriculaIds + ")", 0, Integer.MAX_VALUE)
                .stream()
                .collect(Collectors.toMap(
                        curricula -> curricula.CurriculaID,
                        curricula -> Pair.of(curricula.SubjectID, DateTimeUtils.format(curricula.StartTime, "dd/MM/yyyy") + " - " + DateTimeUtils.format(curricula.EndTime, "dd/MM/yyyy"))
                ));
    }
    
    private Map<Integer, String> getMapAccount(List<Procedures> procedures) {
        String accountIds = procedures.stream().map(procedure -> "" + procedure.AccountID).collect(Collectors.joining(","));
        
        return AccountDao.INST.getList("`AccountID` IN (" + accountIds + ")", 0, Integer.MAX_VALUE).stream()
                .collect(Collectors.toMap(
                        account -> account.AccountID,
                        account -> String.format("%s %s", account.FirstName, account.LastName)
                ));
    }
}
