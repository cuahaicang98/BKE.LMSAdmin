/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.models;

import com.bke.bke.dao.GroupDao;
import com.bke.bke.entity.Account;
import com.bke.bke.entity.Group;
import com.bke.lms.common.Constants;
import com.bke.lms.common.Pagination;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import org.rythmengine.Rythm;

/**
 *
 * @author User
 */
public class GroupModel extends _BaseModel {
    
    public static final GroupModel INST = new GroupModel();
    
    public String buildGroupPage(Account account, int page) {
        Map<String, Object> params = new ConcurrentHashMap<>();
        params.put("title", "Quản lý nhóm");
        params.put("account", account);
        
        List<Group> groups = GroupDao.INST.getList(20 * (page - 1), 20, "`GroupID` ASC");
        params.put("groups", groups);
        params.put("pagination", Pagination.buildPagination(page, GroupDao.INST.count()));

        return Rythm.render(Constants.TEMPLATE_GROUP_PAGE, params);
    }
}
