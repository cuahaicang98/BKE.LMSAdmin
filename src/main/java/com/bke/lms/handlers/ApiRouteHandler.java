/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.handlers;

import com.bke.lms.api.requests.*;
import com.bke.lms.common.Constants;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.regex.Matcher;

/**
 *
 * @author User
 */
public class ApiRouteHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String resource = getResource(exchange);

        switch (resource) {
            case "auths":
                new AuthRequest().doHandler(exchange);
                break;
            case "curriculas":
                new CurriculaRequest().doHandler(exchange);
                break;
            case "curricula-accounts":
                new CurriculaAccountRequest().doHandler(exchange);
                break;
            case "curricula-details":
                new CurriculaDetailRequest().doHandler(exchange);
                break;
            case "curricula-registers":
                new CurriculaRegisterRequest().doHandler(exchange);
                break;
            case "curricula-register-admins":
                new CurriculaRegisterAdminRequest().doHandler(exchange);
                break;
            case "groups":
                new GroupRequest().doHandler(exchange);
                break;
            case "subjects":
                new SubjectRequest().doHandler(exchange);
                break;
        }
    }

    private String getResource(HttpExchange exchange) {
        String URI = exchange.getRequestURI().toString();
        Matcher matcher = Constants.RESOURCE_PATTERN.matcher(URI);

        if (matcher.find() && matcher.find()) {
            return matcher.group(0).replaceFirst("/", "");
        }
        
        return "";
    }
    
}
