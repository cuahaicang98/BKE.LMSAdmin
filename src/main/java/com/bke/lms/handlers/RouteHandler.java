/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.handlers;

import com.bke.lms.common.Constants;
import com.bke.lms.requests.*;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import java.io.IOException;
import java.util.regex.Matcher;

/**
 *
 * @author User
 */
public class RouteHandler implements HttpHandler {

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        String resource = getResource(exchange);

        switch (resource) {
            case "":
                new HomeRequest().doHandler(exchange);
                break;
            case "account":
                new AccountRequest().doHandler(exchange);
                break;
            case "curricula":
                new CurriculaRequest().doHandler(exchange);
                break;
            case "curricula-detail":
                new CurriculaDetailRequest().doHandler(exchange);
                break;
            case "curricula-register":
                new CurriculaRegisterRequest().doHandler(exchange);
                break;
            case "curricula-register-admin":
                new CurriculaRegisterAdminRequest().doHandler(exchange);
                break;
            case "forgot-password":
                new ForgotPasswordRequest().doHandler(exchange);
                break;
            case "group":
                new GroupRequest().doHandler(exchange);
                break;
            case "login":
                new LoginRequest().doHandler(exchange);
                break;
            case "profile":
                new ProfileRequest().doHandler(exchange);
                break;
            case "resource":
                new StaticRequest().doHandler(exchange);
                break;
            case "subject":
                new SubjectRequest().doHandler(exchange);
                break;
            case "time-table":
                new TimeTableRequest().doHandler(exchange);
                break;
            default:
                new ErrorRequest().doHandler(exchange);
        }
    }

    public static String getResource(HttpExchange exchange) {
        String URI = exchange.getRequestURI().toString();
        Matcher matcher = Constants.RESOURCE_PATTERN.matcher(URI);

        String resource = matcher.find() ? matcher.group(0) : "";

        return resource.replaceFirst("/", "");
    }

}
