/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.requests;

import com.bke.lms.models.ErrorModel;
import com.sun.net.httpserver.HttpExchange;

/**
 *
 * @author User
 */
public class ErrorRequest extends _BaseRequest {

    @Override
    public void doHandler(HttpExchange exchange) {
        doProcess(exchange);
    }

    @Override
    public void doProcess(HttpExchange exchange) {
        responseHtml(exchange, 200, ErrorModel.INST.buildErrorPage());
    }
}
