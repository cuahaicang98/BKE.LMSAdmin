/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.requests;

import com.bke.lms.models.StaticModel;
import com.sun.net.httpserver.HttpExchange;
import org.apache.commons.lang3.tuple.Pair;

/**
 *
 * @author User
 */
public class StaticRequest extends _BaseRequest {

    @Override
    public void doHandler(HttpExchange exchange) {
        doProcess(exchange);
    }

    @Override
    public void doProcess(HttpExchange exchange) {
        Pair<String, byte[]> resource = StaticModel.INST.getResource(exchange.getRequestURI().toString());
        response(exchange, resource.getLeft(), 200, resource.getRight());
    }
}
