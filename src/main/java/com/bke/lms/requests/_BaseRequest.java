/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.requests;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.entity.Account;
import com.bke.common.utils.CommonUtils;
import com.bke.common.utils.ConvertUtils;
import com.bke.common.utils.SessionUtils;
import com.bke.lms.common.Constants;
import com.bke.lms.utils.ExchangeUtils;
import com.bke.lms.handlers.RouteHandler;
import com.bke.lms.models.ErrorModel;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public abstract class _BaseRequest {

    public Account account;
    public int page;

    private final List<String> IGNORE_SESSION_RESOURCES = Arrays.asList("resource", "login", "forgot-password");
    private final List<String> NON_SESSION_RESOURCES = Arrays.asList("login");

    public abstract void doProcess(HttpExchange exchange);

    public void doHandler(HttpExchange exchange) {
        try {
            page = ConvertUtils.toInteger(ExchangeUtils.getParamterMap(exchange).get("page"), 1);
            account = ExchangeUtils.getAccount(exchange);

            if (account == null && !IGNORE_SESSION_RESOURCES.contains(RouteHandler.getResource(exchange))) {
                redirect(exchange, Constants.HOST + "/login");
                return;
            }
            
            if (account != null && NON_SESSION_RESOURCES.contains(RouteHandler.getResource(exchange))) {
                redirect(exchange, Constants.HOST);
                return;
            }

            doProcess(exchange);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            responseHtml(exchange, 200, ErrorModel.INST.buildErrorPage());
        }
    }

    private void redirect(HttpExchange exchange, String url) {
        try {
            Headers headers = exchange.getResponseHeaders();
            headers.set("Location", url);

            exchange.sendResponseHeaders(302, 0);
        } catch (IOException e) {
            Logger.getLogger(_BaseRequest.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    protected void responseHtml(HttpExchange exchange, int status, String response) {
        response(exchange, "text/html", status, response.getBytes(StandardCharsets.UTF_8));
    }

    protected void responseHtml(HttpExchange exchange, int status, byte[] response) {
        response(exchange, "text/html", status, response);
    }

    protected void response(HttpExchange exchange, String contentType, int status, byte[] response) {
        try {
            try (OutputStream output = exchange.getResponseBody()) {
                exchange.getResponseHeaders().add("Content-Type", contentType);
                exchange.sendResponseHeaders(status, response.length);
                output.write(response);
                output.flush();
            }
        } catch (IOException e) {
            Logger.getLogger(_BaseRequest.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
