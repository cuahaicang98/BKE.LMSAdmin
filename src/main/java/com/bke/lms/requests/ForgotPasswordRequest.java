/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.requests;

import com.bke.lms.models.ForgotPasswordModel;
import com.sun.net.httpserver.HttpExchange;

/**
 *
 * @author User
 */
public class ForgotPasswordRequest extends _BaseRequest {

    @Override
    public void doProcess(HttpExchange exchange) {
        responseHtml(exchange, 200, ForgotPasswordModel.INST.buildForgotPasswordPage());
    }
}
