/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.bke.lms.utils;

import com.bke.bke.dao.AccountDao;
import com.bke.bke.entity.Account;
import com.bke.common.utils.CommonUtils;
import com.bke.common.utils.SessionUtils;
import com.sun.net.httpserver.HttpExchange;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author User
 */
public class ExchangeUtils {
    
    public static Account getAccount(HttpExchange exchange) {
        String cookie = exchange.getRequestHeaders().getFirst("Cookie");
        
        if (CommonUtils.isEmpty(cookie) || !cookie.contains("bkername=")) {
            return null;
        }
        
        String username = SessionUtils.getValue(cookie.substring(cookie.indexOf("bkername=") + "bkername=".length()));
        
        return username == null ? null : AccountDao.INST.getList("`Username` = '" + username + "'", 0, 1).get(0);
    }

    public static Map<String, String> getParamterMap(HttpExchange exchange) {
        String query = exchange.getRequestURI().getQuery();

        if (query == null) {
            query = getBodyRequest(exchange).replaceAll("\\+", " ");
        }

        Map<String, String> result = new HashMap<>();

        for (String param : query.split("&")) {
            String[] entry = param.split("=");

            if (entry.length > 1) {
                result.put(entry[0], entry[1]);
            } else {
                result.put(entry[0], "");
            }
        }

        return result;
    }
    
    public static String getBodyRequest(HttpExchange exchange)  {
        try {
            InputStreamReader isr = new InputStreamReader(exchange.getRequestBody(), "UTF-8");
            BufferedReader br = new BufferedReader(isr);
            
            int b;
            StringBuilder buf = new StringBuilder(512);
            while ((b = br.read()) != -1) {
                buf.append((char) b);
            }
            
            return URLDecoder.decode(buf.toString(), "UTF-8");
        } catch (IOException ex) {
            return "";
        }
    }
}
