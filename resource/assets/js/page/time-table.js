const $input = $('#html5-week-input');

$input.change(() => {
    const url = new URL(window.location.href);
    url.searchParams.set('time', $input.val());

    window.location.href = url.href;
});
