const modalCenter = $('#modalCenter');
const modalTitle = $('#modalCenterTitle');
const modalSubmitBtn = $('#subjectSubmitBtn');
const addSubjectBtn = $('#addSubjectBtn');

var isEditMode = false;

modalSubmitBtn.click(e => {
    $.post(`/api/subjects/post/${isEditMode ? 'update' : 'create'}`, {
        SubjectID: $('#SubjectID').val(),
        Name: $('#Name').val(),
        Description: $('#Description').val(),
        ParentID: $('#ParentID').val(),
        PreRequisites: $('#PreRequisites').val(),
        Enabled: $('#Enabled').is(":checked") ? 1 : 0,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});

addSubjectBtn.click(e => {
    isEditMode = false;

    modalTitle.text('Thêm lớp học');
    modalSubmitBtn.text('Thêm lớp học');

    $('#SubjectID').val('');
    $('#Name').val('');
    $('#Description').val('');
    $('#ParentID').val('');
    $('#PreRequisites').val('');
    $('#Enabled').prop('checked', true);
});

$('.subjectEditBtn').click(e => {
    isEditMode = true;

    modalTitle.text('Sửa lớp học');
    modalSubmitBtn.text('Sửa lớp học');

    const subject = $($(e.target).closest('tr')[0]).find('td');
    $('#SubjectID').val($(subject[0]).text())
    $('#Name').val($(subject[1]).text());
    $('#Description').val($(subject[2]).text());
    $('#ParentID').val($(subject[3]).text());
    $('#PreRequisites').val($(subject[4]).text());
    $('#Enabled').prop('checked', $(subject[5]).text() === 'true');
});

$('.subjectDeleteBtn').click(e => {
    const subjectId = $(e.target).data('subjectid');

    $.post('/api/subjects/post/delete', {
        SubjectID: subjectId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});
