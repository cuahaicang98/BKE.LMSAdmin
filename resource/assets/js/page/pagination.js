$('.page-link').click(e => {
    const page = $(e.target).data('page');

    const url = new URL(window.location.href);
    url.searchParams.set('page', page);

    window.location.href = url.href;
});
