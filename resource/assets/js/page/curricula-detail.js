const modalCenter = $('#modalCenter');
const modalTitle = $('#modalCenterTitle');
const modalSubmitBtn = $('#curriculaSubmitBtn');
const addCurriculaBtn = $('#addCurriculaBtn');

var isEditMode = false;
var curriculaDetailId = 0;

modalSubmitBtn.click(e => {
    const body = {
        CurriculaID: $('#CurriculaID').val(),
        Slot: $('#Slot').val(),
        StartTime: $('#StartTime').val(),
        Note: $('#Note').val(),
    };

    if (curriculaDetailId) {
        body.CurriculaDetailID = curriculaDetailId;
    }

    $.post(`/api/curricula-details/post/${isEditMode ? 'update' : 'create'}`, body, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});

addCurriculaBtn.click(e => {
    isEditMode = false;
    curriculaDetailId = 0;

    modalTitle.text('Thêm chương trình');
    modalSubmitBtn.text('Thêm chương trình');

    $('#SubjectID').val('');
    $('#StartTime').val('');
    $('#EndTime').val('');
    $('#TotalSlot').val('');
    $('#SlotSchedule').val('');
});

$('.curriculaEditBtn').click(e => {
    isEditMode = true;

    modalTitle.text('Sửa chương trình');
    modalSubmitBtn.text('Sửa chương trình');

    const curricula = $($(e.target).closest('tr')[0]).find('td');
    curriculaDetailId = $(curricula[0]).text();
    $('#CurriculaID').val($(curricula[1]).data('curriculaid'));
    $('#Slot').val($(curricula[2]).text());
    $('#StartTime').val(formatDate($(curricula[3]).text()));
    $('#Note').val($(curricula[4]).text());
});

$('.curriculaDeleteBtn').click(e => {
    const curriculaDetailId = $(e.target).data('curriculadetailid');

    $.post('/api/curricula-details/post/delete', {
        CurriculaDetailID: curriculaDetailId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});

const formatDate = (date) => {
    const split = date.split(' ');
    const hourSplit = split[0].split(':');
    const dateSplit = split[1].split('/');

    return dateSplit[2] + '-' + dateSplit[1] + '-' + dateSplit[0] + 'T' + hourSplit[0] + ':' + hourSplit[1];
}
