$('.approve').click(e => {
    const targetId = $(e.target).data('targetid');

    $.post('/api/curricula-register-admins/post/approve', {
        TargetID: targetId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            alert('Duyệt học phần thành công');
            window.location.reload();
        } else {
            alert('Có lỗi xảy ra!');
        }
    });
});

$('.reject').click(e => {
    const targetId = $(e.target).data('targetid');

    $.post('/api/curricula-register-admins/post/reject', {
        TargetID: targetId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            alert('Ẩn học phần thành công');
            window.location.reload();
        } else {
            alert('Có lỗi xảy ra!');
        }
    });
});