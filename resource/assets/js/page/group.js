const modalCenter = $('#modalCenter');
const modalTitle = $('#modalCenterTitle');
const modalSubmitBtn = $('#groupSubmitBtn');
const addGroupBtn = $('#addGroupBtn');

var isEditMode = false;
var groupId = 0;

modalSubmitBtn.click(e => {
    const body = {
        Name: $('#Name').val(),
        Description: $('#Description').val(),
    };

    if (groupId) {
        body.GroupID = groupId;
    }

    $.post(`/api/groups/post/${isEditMode ? 'update' : 'create'}`, body, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});

addGroupBtn.click(e => {
    isEditMode = false;
    groupId = 0;

    modalTitle.text('Thêm nhóm');
    modalSubmitBtn.text('Thêm nhóm');

    $('#Name').val('');
    $('#Description').val('');
});

$('.groupEditBtn').click(e => {
    isEditMode = true;

    modalTitle.text('Sửa nhóm');
    modalSubmitBtn.text('Sửa nhóm');

    const group = $($(e.target).closest('tr')[0]).find('td');
    groupId = $(group[0]).text();
    $('#Name').val($(group[1]).text());
    $('#Description').val($(group[2]).text());
});

$('.groupDeleteBtn').click(e => {
    const groupId = $(e.target).data('groupid');

    $.post('/api/groups/post/delete', {
        GroupID: groupId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});
