$('#logoutBtn').click(e => {
    document.cookie = "bkername=";
    window.location.href = "/login";
});

$('#loginBtn').click(e => {
    $.post('/api/auths/post/login', {
        Username: $('#username').val(),
        Password: $('#password').val(),
    }, (data, status) => {
        if (status === 'success' && data.data) {
            document.cookie = "bkername=" + data.data;
            window.location.href = "/";
        } else {
            alert('Tài khoản hoặc mật khẩu không chính xác!');
        }
    });
});
