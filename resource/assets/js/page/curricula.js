const modalCenter = $('#modalCenter');
const modalTitle = $('#modalCenterTitle');
const modalAccountTitle = $('#modalAccountTitle');
const modalAccountBody = $('#modalAccountBody');
const modalSubmitBtn = $('#curriculaSubmitBtn');
const addCurriculaBtn = $('#addCurriculaBtn');

var isEditMode = false;
var curriculaId = 0;
var curriculaAccountType = 0;

modalSubmitBtn.click(e => {
    const body = {
        SubjectID: $('#SubjectID').val(),
        StartTime: $('#StartTime').val(),
        EndTime: $('#EndTime').val(),
        TotalSlot: $('#TotalSlot').val(),
        SlotSchedule: $('#SlotSchedule').val(),
    };

    if (curriculaId) {
        body.CurriculaID = curriculaId;
    }

    $.post(`/api/curriculas/post/${isEditMode ? 'update' : 'create'}`, body, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});

addCurriculaBtn.click(e => {
    isEditMode = false;
    curriculaId = 0;

    modalTitle.text('Thêm chương trình');
    modalSubmitBtn.text('Thêm chương trình');

    $('#SubjectID').val('');
    $('#StartTime').val('');
    $('#EndTime').val('');
    $('#TotalSlot').val('');
    $('#SlotSchedule').val('');
});

$('.curriculaEditBtn').click(e => {
    isEditMode = true;

    modalTitle.text('Sửa chương trình');
    modalSubmitBtn.text('Sửa chương trình');

    const curricula = $($(e.target).closest('tr')[0]).find('td');
    curriculaId = $(curricula[0]).text();
    $('#SubjectID').val($(curricula[1]).text());
    $('#StartTime').val(formatDate($(curricula[2]).text()));
    $('#EndTime').val(formatDate($(curricula[3]).text()));
    $('#TotalSlot').val($(curricula[4]).text());
    $('#SlotSchedule').val($(curricula[5]).text());
});

$('.curriculaDeleteBtn').click(e => {
    const curriculaId = $(e.target).data('curriculaid');

    $.post('/api/curriculas/post/delete', {
        CurriculaID: curriculaId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            window.location.reload();
        }
    });
});

$('.teacherBtn').click(e => {
    curriculaAccountType = 2;
    setModalAccountBody(e, curriculaAccountType);
});

$('.studentBtn').click(e => {
    curriculaAccountType = 1;
    setModalAccountBody(e, curriculaAccountType);
});

$('#accountSubmitBtn').click(e => {
    const accountInputs = modalAccountBody.find('input');
    const accountIds = [].slice.call(accountInputs).map(input => input.checked && input.id.replaceAll('account-', '')).filter(Boolean);
    
    $.post('/api/curricula-accounts/post/set-accounts', {
        CurriculaID: curriculaId,
        AccountIDs: accountIds.join(),
        Type: curriculaAccountType,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            alert('Cập nhật thành công!');
            window.location.reload();
        }
    });
});

const setModalAccountBody = (e, type) => {
    const curricula = $($(e.target).closest('tr')[0]).find('td');
    curriculaId = $(curricula[0]).text();
    const subjectId = $(curricula[1]).text();

    modalAccountTitle.text((type === 1 ? 'Học viên: ' : 'Giảng viên: ') + subjectId);
    modalAccountBody.find('input').prop('checked', false);

    $.get(`/api/curricula-accounts/get/items?CurriculaID=${curriculaId}&Type=${type}`, (data) => {
        const accountIds = JSON.parse(data.data).map(d => d.AccountID);
        accountIds.forEach(accountId => $(`#account-${accountId}`).prop('checked', true));
    });
}

const formatDate = (date) => {
    const split = date.split('/');
    return split[2] + '-' + split[1] + '-' + split[0];
}
