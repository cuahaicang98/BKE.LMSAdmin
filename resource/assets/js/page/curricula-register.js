$('.register').click(e => {
    const curriculaId = $(e.target).data('curriculaid');

    $.post('/api/curricula-registers/post/register', {
        CurriculaID: curriculaId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            alert('Đăng kí học phần thành công');
            window.location.reload();
        } else {
            alert('Có lỗi xảy ra!');
        }
    });
});

$('.unregister').click(e => {
    const curriculaId = $(e.target).data('curriculaid');

    $.post('/api/curricula-registers/post/unregister', {
        CurriculaID: curriculaId,
    }, (data, status) => {
        if (status === 'success' && data.data) {
            alert('Hủy đăng kí học phần thành công');
            window.location.reload();
        } else {
            alert('Có lỗi xảy ra!');
        }
    });
});